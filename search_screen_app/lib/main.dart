import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:search_screen_app/model/masterclass_contents.dart';
import 'package:search_screen_app/model/top_vendors.dart';
import 'package:search_screen_app/model/trending_profiles.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SearchView(),
    );
  }
}

class SearchView extends StatefulWidget {
  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Search',
          style: TextStyle(
              color: Color(0xFF5C5C5C),
              fontSize: 16,
              fontWeight: FontWeight.w600),
        ),
        actions: [
          IconButton(
              icon: Icon(
                Icons.search,
                color: Color(0xFF5C5C5C),
              ),
              onPressed: () {
                showSearch(context: context, delegate: SearchDetails());
              }),
        ],
      ),
      backgroundColor: Color(0xFFD8EAFA),
    );
  }
}

class SearchDetails extends SearchDelegate<String> {
  final searchSuggestion = [
    'MasterClass',
    'Profiles',
    'Products',
    'Vendors',
    'Make up',
    'Dermetology',
    'Hair Care',
    'Skin Care',
    'Fashion',
    'Photography',
    'Eyes',
    'Lips',
    'Legs',
    'Baking',
  ];

  final defaultSuggestion = ['MasterClass', 'Profiles', 'Products', 'Vendors'];

  final trendingTags = [
    'Make up',
    'Dermetology',
    'Hair Care',
    'Skin Care',
    'Fashion',
    'Photography',
    'Eyes',
    'Lips',
    'Legs',
    'Baking',
  ];

  List<TopVendors> topVendorsList = [
    TopVendors(
        vendorsImage:
            'https://www.qcmakeupacademy.com/wp-content/uploads/2015/07/Young-Makeup-Artist-at-work.jpg',
        vendorsName: 'Chitrangadha',
        vendorsProfession: 'Makeup Artist'),
    TopVendors(
        vendorsImage:
            'https://images.unsplash.com/photo-1542038784456-1ea8e935640e?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8cGhvdG9ncmFwaHl8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80',
        vendorsName: 'Maunde Peters',
        vendorsProfession: 'Photographers'),
    TopVendors(
        vendorsImage:
            'https://inspirationsandcelebrations.net/wp-content/uploads/2017/10/Behind-The-Scenes-Beauty-A-Celebrity-Hair-Stylist-Shares-Her-On-Set-Hair-Styling-Secrets-1140x560.jpg',
        vendorsName: 'Jean Lindsey',
        vendorsProfession: 'Hair Stylist'),
    TopVendors(
        vendorsImage:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRy-x4hdliQnNYQAehMSpyliIH8Q8QJhDqWvQ&usqp=CAU',
        vendorsName: 'Minnie Collins',
        vendorsProfession: 'Fashion Expert'),
  ];

  List<TrendingProfiles> trendingProfilesList = [
    TrendingProfiles(
        profileImage:
            'https://www.qcmakeupacademy.com/wp-content/uploads/2015/07/Young-Makeup-Artist-at-work.jpg',
        profileName: 'Chitrangadha',
        profileLikes: '1245 Likes'),
    TrendingProfiles(
        profileImage:
            'https://images.unsplash.com/photo-1542038784456-1ea8e935640e?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8cGhvdG9ncmFwaHl8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80',
        profileName: 'Maunde Peters',
        profileLikes: '4.5 Likes'),
    TrendingProfiles(
        profileImage:
            'https://inspirationsandcelebrations.net/wp-content/uploads/2017/10/Behind-The-Scenes-Beauty-A-Celebrity-Hair-Stylist-Shares-Her-On-Set-Hair-Styling-Secrets-1140x560.jpg',
        profileName: 'Jean Lindsey',
        profileLikes: '7895 Likes'),
    TrendingProfiles(
        profileImage:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRy-x4hdliQnNYQAehMSpyliIH8Q8QJhDqWvQ&usqp=CAU',
        profileName: 'Minnie Collins',
        profileLikes: '12.3 Likes'),
    TrendingProfiles(
        profileImage:
            'https://inspirationsandcelebrations.net/wp-content/uploads/2017/10/Behind-The-Scenes-Beauty-A-Celebrity-Hair-Stylist-Shares-Her-On-Set-Hair-Styling-Secrets-1140x560.jpg',
        profileName: 'Olivia Cohen',
        profileLikes: '1245 Likes'),
    TrendingProfiles(
        profileImage:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRy-x4hdliQnNYQAehMSpyliIH8Q8QJhDqWvQ&usqp=CAU',
        profileName: 'Sophia Frank',
        profileLikes: '4.5 Likes'),
  ];

  List<MasterClass> masterclassList = [
    MasterClass(
        masterClassImage:
            'https://inspirationsandcelebrations.net/wp-content/uploads/2017/10/Behind-The-Scenes-Beauty-A-Celebrity-Hair-Stylist-Shares-Her-On-Set-Hair-Styling-Secrets-1140x560.jpg',
        masterclassDetails: 'Live Course - How to learn the art of hairstyling',
        masterclassDate: '27th - 30th July',
        masterclassTime: '14:30 - 15:30'),

    MasterClass(
        masterClassImage:
        'https://www.qcmakeupacademy.com/wp-content/uploads/2015/07/Young-Makeup-Artist-at-work.jpg',
        masterclassDetails: 'Live Course - How to learn the art of makeup',
        masterclassDate: '27th - 30th July',
        masterclassTime: '14:30 - 15:30'),

    MasterClass(
        masterClassImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ5VPCTU-cy2A8JlTsINa7uEL8gVfJ-DmQNNQ&usqp=CAU',
        masterclassDetails: 'Live Course - How to learn the art of mehandi',
        masterclassDate: '27th - 30th July',
        masterclassTime: '14:30 - 15:30'),

    MasterClass(
        masterClassImage:
        'https://images.unsplash.com/photo-1542038784456-1ea8e935640e?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8cGhvdG9ncmFwaHl8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80',
        masterclassDetails: 'Live Course - How to learn the art of mehandi',
        masterclassDate: '27th - 30th July',
        masterclassTime: '14:30 - 15:30'),

    MasterClass(
        masterClassImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRy-x4hdliQnNYQAehMSpyliIH8Q8QJhDqWvQ&usqp=CAU',
        masterclassDetails: 'Live Course - How to learn the art of mehandi',
        masterclassDate: '27th - 30th July',
        masterclassTime: '14:30 - 15:30'),

    MasterClass(
        masterClassImage:
        'https://inspirationsandcelebrations.net/wp-content/uploads/2017/10/Behind-The-Scenes-Beauty-A-Celebrity-Hair-Stylist-Shares-Her-On-Set-Hair-Styling-Secrets-1140x560.jpg',
        masterclassDetails: 'Live Course - How to learn the art of mehandi',
        masterclassDate: '27th - 30th July',
        masterclassTime: '14:30 - 15:30'),

    MasterClass(
        masterClassImage:
        'https://inspirationsandcelebrations.net/wp-content/uploads/2017/10/Behind-The-Scenes-Beauty-A-Celebrity-Hair-Stylist-Shares-Her-On-Set-Hair-Styling-Secrets-1140x560.jpg',
        masterclassDetails: 'Live Course - How to learn the art of mehandi',
        masterclassDate: '27th - 30th July',
        masterclassTime: '14:30 - 15:30'),

    MasterClass(
        masterClassImage:
        'https://inspirationsandcelebrations.net/wp-content/uploads/2017/10/Behind-The-Scenes-Beauty-A-Celebrity-Hair-Stylist-Shares-Her-On-Set-Hair-Styling-Secrets-1140x560.jpg',
        masterclassDetails: 'Live Course - How to learn the art of mehandi',
        masterclassDate: '27th - 30th July',
        masterclassTime: '14:30 - 15:30'),
  ];

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = '';
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return null;
  }

  @override
  Widget buildResults(BuildContext context) {
    return null;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty ? defaultSuggestion : searchSuggestion;

    return Container(
      height: MediaQuery.of(context).size.height,
      child: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(left: 5, right: 5, top: 10, bottom: 10),
              child: StaggeredGridView.countBuilder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                mainAxisSpacing: 5,
                crossAxisSpacing: 5,
                crossAxisCount: 2,
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 4),
                staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
                itemCount: suggestionList.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    elevation: 3.0,
                    child: ListTile(
                      tileColor: Colors.white,
                      leading: Icon(
                        Icons.search,
                        size: 20.0,
                        color: Colors.grey.shade800,
                      ),
                      title: Align(
                        alignment: Alignment(-1.8, 0),
                        child: Text(
                          suggestionList[index],
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey.shade700,
                            fontSize: 12,
                          ),
                          maxLines: 1,
                        ),
                      ),
                      dense: true,
                    ),
                  );
                },
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(left: 12.0, top: 20.0, bottom: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Trending Tags',
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                  StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.only(top: 15),
                    crossAxisSpacing: 5,
                    crossAxisCount: 3,
                    staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
                    itemCount: trendingTags.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        elevation: 2.0,
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          child: Text(
                            trendingTags[index],
                            style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.grey.shade700,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(left: 12.0, top: 20.0, bottom: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Top Vendors',
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                  StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.only(top: 15, right: 5),
                    crossAxisSpacing: 5,
                    crossAxisCount: 2,
                    staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
                    itemCount: topVendorsList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ProfilesCardDesign(
                        topVendorsList: topVendorsList,
                        circleImage: topVendorsList[index].vendorsImage,
                        profileName: topVendorsList[index].vendorsName,
                        profileProfession:
                            topVendorsList[index].vendorsProfession,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(left: 12.0, top: 20.0, bottom: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Trending Profiles',
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                  StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.only(top: 15, right: 5),
                    crossAxisSpacing: 5,
                    crossAxisCount: 2,
                    staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
                    itemCount: trendingProfilesList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ProfilesCardDesign(
                        trendingProfilesList: trendingProfilesList,
                        circleImage: trendingProfilesList[index].profileImage,
                        profileName: trendingProfilesList[index].profileName,
                        profileProfession:
                            trendingProfilesList[index].profileLikes,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: 240,
              width: 50,
              color: Colors.grey.shade200,
              padding: EdgeInsets.only(top: 15, bottom: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                    child: Text(
                      'Masterclass',
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40.0)),
                          height: double.infinity,
                          width: 140,
                          child: Card(
                            elevation: 2.0,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Image(
                                  image: NetworkImage(
                                      masterclassList[index].masterClassImage),
                                  height: 50,
                                ),
                                SizedBox(
                                  height: 12.0,
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 15.0),
                                  child: Text(
                                    masterclassList[index].masterclassDetails,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                                SizedBox(
                                  height: 12.0,
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 15.0),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.calendar_today_rounded,
                                        color: Colors.pink.shade700,
                                        size: 10,
                                      ),
                                      SizedBox(width: 5.0,),
                                      Text(
                                        masterclassList[index].masterclassDate,
                                        style: TextStyle(
                                            fontSize: 10.0,
                                            fontWeight: FontWeight.w700,
                                            color: Colors.pink.shade600),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 15.0),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.watch_later,
                                        color: Colors.pink.shade700,
                                        size: 10,
                                      ),
                                      SizedBox(width: 5.0,),
                                      Text(
                                        masterclassList[index].masterclassTime,
                                        style: TextStyle(
                                            fontSize: 10.0,
                                            fontWeight: FontWeight.w700,
                                            color: Colors.pink.shade600),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      itemCount: masterclassList.length,
                      scrollDirection: Axis.horizontal,
                      physics: BouncingScrollPhysics(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ProfilesCardDesign extends StatelessWidget {
  ProfilesCardDesign(
      {this.trendingProfilesList,
      this.topVendorsList,
      this.circleImage,
      this.profileName,
      this.profileProfession});

  final List<TrendingProfiles> trendingProfilesList;
  final List<TopVendors> topVendorsList;
  String circleImage;
  String profileName;
  String profileProfession;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3.0,
      child: ListTile(
        tileColor: Colors.white,
        leading: CircleAvatar(
          backgroundImage: NetworkImage(circleImage),
        ),
        title: Align(
          alignment: Alignment(-1.8, 0),
          child: Text(
            profileName,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.grey.shade700,
              fontSize: 12,
            ),
            maxLines: 1,
          ),
        ),
        dense: true,
        subtitle: Text(
          profileProfession,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey.shade600,
            fontSize: 10,
          ),
          maxLines: 1,
        ),
      ),
    );
  }
}
